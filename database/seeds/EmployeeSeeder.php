<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ms_MY');

        for ($i = 1; $i <= 100; $i++) {

            DB::table('employees')->insert([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'company_id' => $faker->numberBetween(1, 10),
                'email' => $faker->email,
                'phone' => $faker->phoneNumber
            ]);
        }
    }
}
