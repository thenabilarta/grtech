<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Eloquent::unguard();

        $hashed = Hash::make('password');

        DB::table('users')->insert([
            'email' =>  'admin@grtech.com.my',
            'password' => $hashed,
        ]);

        DB::table('users')->insert([
            'email' =>  'user@grtech.com.my',
            'password' => $hashed,
        ]);
    }
}
