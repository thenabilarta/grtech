/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Company__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/Company */ "./resources/js/components/Company.js");
/* harmony import */ var _components_Company__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_components_Company__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_Employee__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/Employee */ "./resources/js/components/Employee.js");
/* harmony import */ var _components_Employee__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_Employee__WEBPACK_IMPORTED_MODULE_1__);


$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
  });
});

/***/ }),

/***/ "./resources/js/components/Company.js":
/*!********************************************!*\
  !*** ./resources/js/components/Company.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var company_table;
company_table = $("#company_table").DataTable({
  processing: true,
  serverSide: true,
  order: [[0, "desc"]],
  ajax: {
    url: "/company/data",
    type: "get"
  },
  columns: [{
    data: "id"
  }, {
    data: "name"
  }, {
    data: "email"
  }, {
    data: dataTableCreateLogo
  }, {
    data: dataTableCreateWebsite
  }, {
    data: "action",
    orderable: false
  }]
});

function dataTableCreateLogo(data) {
  var image = "<img class=\"company-logo\" src=\"/storage/".concat(data.logo, "\" />");
  return image;
}

function dataTableCreateWebsite(data) {
  var link = "<a href=\"".concat(data.website, "\">").concat(data.website, "</a>");
  return link;
}

function editForm(companyData) {
  return "\n    <div class=\"form-group\">\n      <div class=\"col-sm-offset-2 col-sm-10\">\n        <label for=\"input-logo-edit\">\n          <img class=\"logo-edit\" src=\"/storage/".concat(companyData.logo, "\">\n        </label>\n        <input type=\"file\" name=\"input-logo-edit\" id=\"input-logo-edit\">\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"inputName\">Name</label>\n      <input type=\"text\" class=\"form-control\" id=\"inputNameCompany\" placeholder=\"Enter name\" value=\"").concat(companyData.name, "\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"inputEmail\">Email</label>\n      <input type=\"email\" value=\"").concat(companyData.email, "\" class=\"form-control\" id=\"inputEmailCompany\" placeholder=\"Enter Email\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"inputWebsite\">Website</label>\n      <input type=\"website\" value=\"").concat(companyData.website, "\" class=\"form-control\" id=\"inputWebsiteCompany\" placeholder=\"Enter Website\">\n    </div>\n    ");
}

var editedInputLogo;
$(document).on("change", "#input-logo-edit", function () {
  var editLogoInput = document.querySelector("#input-logo-edit");
  var fileSource = URL.createObjectURL(editLogoInput.files[0]);
  editedInputLogo = editLogoInput.files[0];
  $(".logo-edit").attr("src", fileSource);
});
$(document).on("click", ".company-action-edit", function () {
  var id = $(this).attr("id").split("-")[0];
  axios.get("/company/show/".concat(id)).then(function (res) {
    var companyData = res.data;
    var dialog = bootbox.dialog({
      title: "Edit Company",
      message: editForm(companyData),
      size: "large",
      buttons: {
        cancel: {
          label: '<i class="fa fa-times"></i> Cancel'
        },
        ok: {
          label: '<i class="fa fa-check"></i> Confirm',
          className: "test-button",
          callback: function callback() {
            var name = $("#inputNameCompany").val();
            var email = $("#inputEmailCompany").val();
            var website = $("#inputWebsiteCompany").val();
            var logo = editedInputLogo;
            var formData = new FormData();
            formData.append("file", logo);
            formData.append("name", name);
            formData.append("email", email);
            formData.append("website", website);
            var config = {
              headers: {
                "content-type": "multipart/form-data"
              }
            };
            axios.post("/company/".concat(id, "/store"), formData, config).then(function (res) {
              if (res.data.status === "error") {
                toastr.error(res.data.message, "Error", {
                  timeOut: 2000,
                  positionClass: "toast-top-right"
                });
              }

              if (res.data.status === "success") {
                toastr.success("Company data has been updated", "Success", {
                  timeOut: 2000,
                  positionClass: "toast-top-right"
                });
                company_table.ajax.reload();
                dialog.modal("hide");
              }
            });
            return false;
          }
        }
      }
    });
  });
});
$(document).on("click", ".company-action-delete", function () {
  var id = $(this).attr("id").split("-")[0];
  bootbox.dialog({
    title: "Delete media",
    message: "Do you want to delete this company? This action cannot be undone.",
    size: "large",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancel'
      },
      ok: {
        label: '<i class="fa fa-check"></i> Confirm',
        callback: function callback() {
          axios.post("/company/".concat(id, "/delete")).then(function (res) {
            if (res.data.status === "success") {
              toastr.success("Company data has been deleted", "Success", {
                timeOut: 2000,
                positionClass: "toast-top-right"
              });
              company_table.ajax.reload();
            }
          });
        }
      }
    }
  });
});
$(document).on("click", ".create-company-data", function () {
  window.location.href = "/company/create";
});

/***/ }),

/***/ "./resources/js/components/Employee.js":
/*!*********************************************!*\
  !*** ./resources/js/components/Employee.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var employee_table;
employee_table = $("#employee_table").DataTable({
  processing: true,
  serverSide: true,
  order: [[0, "desc"]],
  ajax: {
    url: "/employee/data",
    type: "get"
  },
  columns: [{
    data: "id"
  }, {
    data: "full_name"
  }, {
    data: dataTableCreateCompanyLink
  }, {
    data: "email"
  }, {
    data: "phone"
  }, {
    data: "action",
    orderable: false
  }]
});

function dataTableCreateCompanyLink(data) {
  var link = "<a href=\"#\" id=\"".concat(data.company_id, "-company-modal\"; class=\"open-company-modal\">").concat(data.company_name, "</a>");
  return link;
}

function companyInfo(companyData) {
  return "\n  <img class=\"company-logo-medium\" src=\"/storage/".concat(companyData.logo, "\" />\n  <table class=\"table\">\n  <tbody>\n    <tr>\n      <th scope=\"row\">Name</th>\n      <td>").concat(companyData.name, "</td>\n    </tr>\n    <tr>\n      <th scope=\"row\">Email</th>\n      <td>").concat(companyData.email, "</td>\n    </tr>\n    <tr>\n      <th scope=\"row\">Website</th>\n      <td>").concat(companyData.website, "</td>\n    </tr>\n  </tbody>\n</table>");
}

$(document).on("click", ".open-company-modal", function () {
  var id = $(this).attr("id").split("-")[0];
  axios.get("/company/show/".concat(id)).then(function (res) {
    var companyData = res.data;
    var dialog = bootbox.dialog({
      title: "Company Profile",
      message: companyInfo(companyData),
      size: "large",
      buttons: {
        Close: {}
      }
    });
  });
});

function editForm(employeeData, companyData) {
  var string;
  var employeeCompanyID = employeeData.company_id;

  if (employeeCompanyID === null) {
    string += "<option selected value=\"0\">No Company</option>";
    companyData.forEach(function (c) {
      string += "<option value=\"".concat(c.id, "\">").concat(c.name, "</option>");
    });
  }

  if (employeeCompanyID !== null) {
    string += "<option value=\"0\">No Company</option>";
    companyData.forEach(function (c) {
      if (employeeCompanyID === c.id) {
        string += "<option selected value=\"".concat(c.id, "\">").concat(c.name, "</option>");
      } else {
        string += "<option value=\"".concat(c.id, "\">").concat(c.name, "</option>");
      }
    });
  }

  return "\n    <div class=\"form-row\">\n      <div class=\"col-md-6 mb-3\">\n        <label for=\"inputFirstNameEmployee\">First name</label>\n        <input type=\"text\" class=\"form-control\" id=\"inputFirstNameEmployee\" placeholder=\"First name\" value=\"".concat(employeeData.first_name, "\" required>\n        <div class=\"valid-tooltip\">\n          Looks good!\n        </div>\n      </div>\n      <div class=\"col-md-6 mb-3\">\n        <label for=\"inputLastNameEmployee\">Last name</label>\n        <input type=\"text\" class=\"form-control\" id=\"inputLastNameEmployee\" placeholder=\"Last name\" value=\"").concat(employeeData.last_name, "\" required>\n        <div class=\"valid-tooltip\">\n          Looks good!\n        </div>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <label>Company</label>\n      <select id=employeesCompanyID class=\"form-control\">\n        ").concat(string, "\n      </select>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"inputEmailEmployee\">Email</label>\n      <input type=\"email\" value=\"").concat(employeeData.email, "\" class=\"form-control\" id=\"inputEmailEmployee\" placeholder=\"Enter Email\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"inputPhoneEmployee\">Phone</label>\n      <input type=\"website\" value=\"").concat(employeeData.phone, "\" class=\"form-control\" id=\"inputPhoneEmployee\" placeholder=\"Enter Website\">\n    </div>\n    ");
}

$(document).on("click", ".employee-action-edit", function () {
  var id = $(this).attr("id").split("-")[0];
  var getEmployee = axios.get("/employee/show/".concat(id));
  var getCompany = axios.get("/company/list");
  axios.all([getEmployee, getCompany]).then(axios.spread(function (res1, res2) {
    var employeeData = res1.data;
    var companyData = res2.data;
    var dialog = bootbox.dialog({
      title: "Edit Employee",
      message: editForm(employeeData, companyData),
      size: "large",
      buttons: {
        cancel: {
          label: '<i class="fa fa-times"></i> Cancel'
        },
        ok: {
          label: '<i class="fa fa-check"></i> Confirm',
          callback: function callback() {
            var employeesCompanyID = $("#employeesCompanyID").find(":selected").val();
            var firstName = $("#inputFirstNameEmployee").val();
            var lastName = $("#inputLastNameEmployee").val();
            var email = $("#inputEmailEmployee").val();
            var phone = $("#inputPhoneEmployee").val();
            axios.post("/employee/".concat(id, "/store"), {
              data: {
                employeesCompanyID: employeesCompanyID,
                firstName: firstName,
                lastName: lastName,
                email: email,
                phone: phone
              }
            }).then(function (res) {
              if (res.data.status === "error") {
                toastr.error(res.data.message, "Error", {
                  timeOut: 2000,
                  positionClass: "toast-top-right"
                });
              }

              if (res.data.status === "success") {
                toastr.success("Company data has been updated", "Success", {
                  timeOut: 2000,
                  positionClass: "toast-top-right"
                });
                employee_table.ajax.reload();
                dialog.modal("hide");
              }
            });
            return false;
          }
        }
      }
    });
  }));
});
$(document).on("click", ".employee-action-delete", function () {
  var id = $(this).attr("id").split("-")[0];
  bootbox.dialog({
    title: "Delete media",
    message: "Do you want to delete this employee data? This action cannot be undone.",
    size: "large",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancel'
      },
      ok: {
        label: '<i class="fa fa-check"></i> Confirm',
        callback: function callback() {
          axios.post("/employee/".concat(id, "/delete")).then(function (res) {
            if (res.data.status === "success") {
              toastr.success("Employee data has been deleted", "Success", {
                timeOut: 2000,
                positionClass: "toast-top-right"
              });
              employee_table.ajax.reload();
            }
          });
        }
      }
    }
  });
});
$(document).on("click", ".create-employee-data", function () {
  window.location.href = "/employee/create";
});

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\Users\thena\Desktop\grtech-test\resources\js\app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! C:\Users\thena\Desktop\grtech-test\resources\sass\app.scss */"./resources/sass/app.scss");


/***/ })

/******/ });