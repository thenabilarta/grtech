# Laravel 5.8

## _GRTECH Laravel Test_

### Features

- Add Company
- Add Employee
- Upload Company Logo
- Change Company Logo
- Admin and user route
- Email Notification

## Library

This project uses a number of open source projects to work properly:

- Axios
- jQuery
- Bootsrap
- Bootbox
- Laravel mix
- Sass

## Installation

Install the dependencies and devDependencies and start the server.

```
composer install
php artisan config:cache
php artisan migrate
php artisan db:seed
php artisan serve
```

For javascript developments environments...

```
npm install
npm run watch
```
