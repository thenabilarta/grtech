var employee_table;

employee_table = $("#employee_table").DataTable({
  processing: true,
  serverSide: true,
  order: [[0, "desc"]],
  ajax: {
    url: "/employee/data",
    type: "get",
  },
  columns: [
    {
      data: "id",
    },
    {
      data: "full_name",
    },
    {
      data: dataTableCreateCompanyLink,
    },
    {
      data: "email",
    },
    {
      data: "phone",
    },
    {
      data: "action",
      orderable: false,
    },
  ],
});

function dataTableCreateCompanyLink(data) {
  const link = `<a href="#" id="${data.company_id}-company-modal"; class="open-company-modal">${data.company_name}</a>`;

  return link;
}

function companyInfo(companyData) {
  return `
  <img class="company-logo-medium" src="/storage/${companyData.logo}" />
  <table class="table">
  <tbody>
    <tr>
      <th scope="row">Name</th>
      <td>${companyData.name}</td>
    </tr>
    <tr>
      <th scope="row">Email</th>
      <td>${companyData.email}</td>
    </tr>
    <tr>
      <th scope="row">Website</th>
      <td>${companyData.website}</td>
    </tr>
  </tbody>
</table>`;
}

$(document).on("click", ".open-company-modal", function() {
  const id = $(this)
    .attr("id")
    .split("-")[0];

  axios.get(`/company/show/${id}`).then((res) => {
    const companyData = res.data;
    var dialog = bootbox.dialog({
      title: "Company Profile",
      message: companyInfo(companyData),
      size: "large",
      buttons: {
        Close: {},
      },
    });
  });
});

function editForm(employeeData, companyData) {
  let string;
  const employeeCompanyID = employeeData.company_id;

  if (employeeCompanyID === null) {
    string += `<option selected value="0">No Company</option>`;

    companyData.forEach((c) => {
      string += `<option value="${c.id}">${c.name}</option>`;
    });
  }

  if (employeeCompanyID !== null) {
    string += `<option value="0">No Company</option>`;

    companyData.forEach((c) => {
      if (employeeCompanyID === c.id) {
        string += `<option selected value="${c.id}">${c.name}</option>`;
      } else {
        string += `<option value="${c.id}">${c.name}</option>`;
      }
    });
  }

  return `
    <div class="form-row">
      <div class="col-md-6 mb-3">
        <label for="inputFirstNameEmployee">First name</label>
        <input type="text" class="form-control" id="inputFirstNameEmployee" placeholder="First name" value="${employeeData.first_name}" required>
        <div class="valid-tooltip">
          Looks good!
        </div>
      </div>
      <div class="col-md-6 mb-3">
        <label for="inputLastNameEmployee">Last name</label>
        <input type="text" class="form-control" id="inputLastNameEmployee" placeholder="Last name" value="${employeeData.last_name}" required>
        <div class="valid-tooltip">
          Looks good!
        </div>
      </div>
    </div>
    <div class="form-group">
      <label>Company</label>
      <select id=employeesCompanyID class="form-control">
        ${string}
      </select>
    </div>
    <div class="form-group">
      <label for="inputEmailEmployee">Email</label>
      <input type="email" value="${employeeData.email}" class="form-control" id="inputEmailEmployee" placeholder="Enter Email">
    </div>
    <div class="form-group">
      <label for="inputPhoneEmployee">Phone</label>
      <input type="website" value="${employeeData.phone}" class="form-control" id="inputPhoneEmployee" placeholder="Enter Website">
    </div>
    `;
}

$(document).on("click", ".employee-action-edit", function() {
  const id = $(this)
    .attr("id")
    .split("-")[0];

  const getEmployee = axios.get(`/employee/show/${id}`);
  const getCompany = axios.get(`/company/list`);
  axios.all([getEmployee, getCompany]).then(
    axios.spread((res1, res2) => {
      const employeeData = res1.data;
      const companyData = res2.data;
      var dialog = bootbox.dialog({
        title: "Edit Employee",
        message: editForm(employeeData, companyData),
        size: "large",
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> Cancel',
          },
          ok: {
            label: '<i class="fa fa-check"></i> Confirm',
            callback: function() {
              const employeesCompanyID = $("#employeesCompanyID")
                .find(":selected")
                .val();
              const firstName = $("#inputFirstNameEmployee").val();
              const lastName = $("#inputLastNameEmployee").val();
              const email = $("#inputEmailEmployee").val();
              const phone = $("#inputPhoneEmployee").val();

              axios
                .post(`/employee/${id}/store`, {
                  data: {
                    employeesCompanyID,
                    firstName,
                    lastName,
                    email,
                    phone,
                  },
                })
                .then((res) => {
                  if (res.data.status === "error") {
                    toastr.error(res.data.message, "Error", {
                      timeOut: 2000,
                      positionClass: "toast-top-right",
                    });
                  }

                  if (res.data.status === "success") {
                    toastr.success(`Company data has been updated`, "Success", {
                      timeOut: 2000,
                      positionClass: "toast-top-right",
                    });
                    employee_table.ajax.reload();
                    dialog.modal("hide");
                  }
                });

              return false;
            },
          },
        },
      });
    })
  );
});

$(document).on("click", ".employee-action-delete", function() {
  const id = $(this)
    .attr("id")
    .split("-")[0];
  bootbox.dialog({
    title: "Delete media",
    message: `Do you want to delete this employee data? This action cannot be undone.`,
    size: "large",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancel',
      },
      ok: {
        label: '<i class="fa fa-check"></i> Confirm',
        callback: function() {
          axios.post(`/employee/${id}/delete`).then((res) => {
            if (res.data.status === "success") {
              toastr.success(`Employee data has been deleted`, "Success", {
                timeOut: 2000,
                positionClass: "toast-top-right",
              });
              employee_table.ajax.reload();
            }
          });
        },
      },
    },
  });
});

$(document).on("click", ".create-employee-data", function() {
  window.location.href = "/employee/create";
});
