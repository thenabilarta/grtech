@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Create Employee Data
                </div>

                <div class="card-body">
                    <form action="/employee/create/save" enctype="multipart/form-data" method="post">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstName">First name</label>
                                <input value="{{ old('firstName') }}" type="text" class="form-control" id="firstName" name="firstName" placeholder="First name">

                                @if ($errors->has('firstName'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstName') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastName">Last name</label>
                                <input value="{{ old('lastName') }}" type="text" class="form-control" name="lastName" id="lastName" placeholder="Last name">

                                @if ($errors->has('lastName'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastName') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Company</label>
                            <select id=company_id name="company_id" class="form-control">
                                <option value="0">No Company</option>
                                @foreach ($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input value="{{ old('email') }}" type="email" name="email" class="form-control" id="email" placeholder="Enter Email">

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input value="{{ old('phone') }}" type="website" name="phone" class="form-control" id="phone" placeholder="Enter Phone Number">

                            @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection