<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\AdminRoute;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['AdminRoute']], function () {
    Route::get('/company', 'CompanyController@index');
    Route::get('/company/create', 'CompanyController@create');
    Route::post('/company/create/save', 'CompanyController@createSave');
    Route::get('/company/show/{id}', 'CompanyController@show');
    Route::post('/company/{id}/store', 'CompanyController@store');
    Route::post('/company/{id}/delete', 'CompanyController@delete');
    Route::get('/company/data', 'CompanyController@data');
    Route::get('/company/list', 'CompanyController@list');

    Route::get('/employee', 'EmployeeController@index');
    Route::get('/employee/create', 'EmployeeController@create');
    Route::post('/employee/create/save', 'EmployeeController@createSave');
    Route::get('/employee/show/{id}', 'EmployeeController@show');
    Route::post('/employee/{id}/delete', 'EmployeeController@delete');
    Route::post('/employee/{id}/store', 'EmployeeController@store');
    Route::get('/employee/data', 'EmployeeController@data');
});
